FROM alpine

RUN apk add icu-dev --update ca-certificates && rm -rf /var/cache/apk/*

COPY bin/gitlab-elasticsearch-indexer /bin/gitlab-elasticsearch-indexer
#ENTRYPOINT ["/bin/gitlab-elasticsearch-indexer"]
